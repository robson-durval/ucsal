<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const USUARIO_ESTUDANTE = 1;
    const USUARIO_PROFESSOR = 2;
    const USUARIO_COORDENADOR = 3;

    public static $perfils = [
        self::USUARIO_ESTUDANTE => 'Aluno',
        self::USUARIO_PROFESSOR => 'Professor',
        self::USUARIO_COORDENADOR => 'Coordenador'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
