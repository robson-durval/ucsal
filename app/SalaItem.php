<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaItem extends Model
{
    protected $table = "salas_itens";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sala_id', 'item_id'
    ];
}
