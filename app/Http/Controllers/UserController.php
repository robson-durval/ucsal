<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
	// retorna a view index com a listagem de todos os usuários
    public function index()
    {
    	$usuarios = User::all();

    	return view('usuario.index')->with(['users' => $usuarios]);
    }

    // cadastra um usuário
    public function cadastrar(Request $request)
    {
    	$usuario = new User;

    	if(!empty($request->all())) {
    		$usuario->fill($request->all());
    		$usuario->password = bcrypt($usuario->password);
    		$usuario->save();
    		return redirect('/usuarios/index');
    	}

    	return view('usuario.cadastrar');
    }

    public function delete($id)
    {
        User::find($id)->delete();
        return redirect('/usuarios/index');
    }
}
