<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class ItensController extends Controller
{
    // retorna a view index com a listagem de todos os usuários
    public function index()
    {
    	$itens = Item::all();

    	return view('itens.index')->with(['itens' => $itens]);
    }

    // cadastra um usuário
    public function cadastrar(Request $request)
    {
    	$item = new Item;

    	if(!empty($request->all())) {
    		$item->fill($request->all());
    		$item->save();
    		return redirect('/itens/index');
    	}

    	return view('itens.cadastrar');
    }

    public function delete($id)
    {
        Item::find($id)->delete();
        return redirect('/itens/index');
    }
}
