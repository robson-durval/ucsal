<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sala;

class SalasController extends Controller
{
    // retorna a view index com a listagem de todos os usuários
    public function index()
    {
    	$salas = Sala::all();

    	return view('salas.index')->with(['salas' => $salas]);
    }

    // cadastra um usuário
    public function cadastrar(Request $request)
    {
    	$sala = new Sala;

    	if(!empty($request->all())) {
    		$sala->fill($request->all());
    		$sala->save();
    		return redirect('/salas/index');
    	}

    	return view('salas.cadastrar');
    }

    public function delete($id)
    {
        Sala::find($id)->delete();
        return redirect('/salas/index');
    }
}
