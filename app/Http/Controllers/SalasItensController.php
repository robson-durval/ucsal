<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalaItem;

class SalasItensController extends Controller
{
    // retorna a view index com a listagem de todos os usuários
    public function index()
    {
    	$salas_itens = SalaItem::all();

    	return view('salas_itens.index')->with(['salas_itens' => $salas_itens]);
    }

    // cadastra um usuário
    public function cadastrar(Request $request)
    {
    	$sala_item = new SalaItem;

    	if(!empty($request->all())) {
    		$sala_item->fill($request->all());
    		$sala_item->save();
    		return redirect('/salas_itens/index');
    	}

    	return view('salas_itens.cadastrar');
    }

    public function delete($id)
    {
        SalaItem::find($id)->delete();
        return redirect('/salas_itens/index');
    }
}
