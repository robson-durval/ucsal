<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ocorrencia;

class OcorrenciasController extends Controller
{
    // retorna a view index com a listagem de todos os usuários
    public function index()
    {
    	$ocorrencias = Ocorrencia::all();

    	return view('ocorrencias.index')->with(['ocorrencias' => $ocorrencias]);
    }

    // cadastra um usuário
    public function cadastrar(Request $request)
    {
    	$ocorrencia = new Ocorrencia;
    	if(!empty($request->all())) {
    		$ocorrencia->fill($request->all());
    		$ocorrencia->criado_por = \Auth::user()->id;
    		$ocorrencia->save();
    		return redirect('/ocorrencias/index');
    	}

        if(\Auth::user()->profile == 1 || \Auth::user()->profile == 2)
    	   return view('ocorrencias.cadastrar');
        else
        return redirect('/ocorrencias/index');
    }

    public function get_items_by_classe(Request $request)
    {
        $salas_itens = \App\SalaItem::where(['sala_id' => $request->get('classe_id')])->get();
        $ids = [];
        foreach ($salas_itens as $key => $value) {
            $ids[] = $value->item_id;
        }
        
        $itens = \App\Item::whereIn('id', $ids)->get();

        $html = '';

        foreach ($itens as $key => $value) {
            $html .= '<option value="'.$value->id.'"> '.$value->nome.' </option>';
        }

        return $html;
    }

    
    public function delete($id)
    {
        Ocorrencia::find($id)->delete();
        return redirect('/ocorrencias/index');
    }
}
