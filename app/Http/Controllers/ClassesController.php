<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes;

class ClassesController extends Controller
{
    // retorna a view index com a listagem de todos os usuários
    public function index()
    {
    	$classes = Classes::all();

    	return view('classes.index')->with(['classes' => $classes]);
    }

    // cadastra um usuário
    public function cadastrar(Request $request)
    {
    	$classe = new Classes;

    	if(!empty($request->all())) {
    		$classe->fill($request->all());
    		$classe->save();
    		return redirect('/classes/index');
    	}

    	return view('classes.cadastrar');
    }

    public function delete($id)
    {
        Classes::find($id)->delete();
        return redirect('/classes/index');
    }
}
