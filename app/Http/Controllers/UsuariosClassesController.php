<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsuariosClasses;

class UsuariosClassesController extends Controller
{
   	// retorna a view index com a listagem de todos os usuários
    public function index()
    {
    	$usuarios_classes = UsuariosClasses::all();

    	return view('usuarios_classes.index')->with(['usuarios_classes' => $usuarios_classes]);
    }

    // cadastra um usuário
    public function cadastrar(Request $request)
    {
    	$usuarios_classe = new UsuariosClasses;

    	if(!empty($request->all())) {
    		$usuarios_classe->fill($request->all());
    		$usuarios_classe->save();
    		return redirect('/usuarios_classes/index');
    	}

    	return view('usuarios_classes.cadastrar');
    }

    public function delete($id)
    {
        UsuariosClasses::find($id)->delete();
        return redirect('/usuarios_classes/index');
    }
}
