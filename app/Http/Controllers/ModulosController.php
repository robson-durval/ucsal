<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modulo;

class ModulosController extends Controller
{
    // retorna a view index com a listagem de todos os usuários
    public function index()
    {
    	$modulos = Modulo::all();

    	return view('modulos.index')->with(['modulos' => $modulos]);
    }

    // cadastra um usuário
    public function cadastrar(Request $request)
    {
    	$modulo = new Modulo;

    	if(!empty($request->all())) {
    		$modulo->fill($request->all());
    		$modulo->save();
    		return redirect('/modulos/index');
    	}

    	return view('modulos.cadastrar');
    }

    public function delete($id)
    {
        Modulo::find($id)->delete();
        return redirect('/modulos/index');
    }
}
