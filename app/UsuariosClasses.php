<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuariosClasses extends Model
{
    protected $table = "usuarios_classes";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario_id', 'classe_id',
    ];
}
