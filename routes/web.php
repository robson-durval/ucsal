<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// grupo de rotas para ações de usuários
Route::group(['prefix' => 'usuarios'], function(){
	Route::get('/index', ['uses' => 'UserController@index']);
	Route::get('/cadastrar', ['uses' => 'UserController@cadastrar']);
	Route::get('/delete/{id}', ['uses' => 'UserController@delete']);
	Route::post('/cadastrar', ['uses' => 'UserController@cadastrar']);
});

Route::group(['prefix' => 'itens'], function(){
	Route::get('/index', ['uses' => 'ItensController@index']);
	Route::get('/cadastrar', ['uses' => 'ItensController@cadastrar']);
	Route::get('/delete/{id}', ['uses' => 'ItensController@delete']);
	Route::post('/cadastrar', ['uses' => 'ItensController@cadastrar']);
});

Route::group(['prefix' => 'modulos'], function(){
	Route::get('/index', ['uses' => 'ModulosController@index']);
	Route::get('/cadastrar', ['uses' => 'ModulosController@cadastrar']);
	Route::get('/delete/{id}', ['uses' => 'ModulosController@delete']);
	Route::post('/cadastrar', ['uses' => 'ModulosController@cadastrar']);
});

Route::group(['prefix' => 'salas'], function(){
	Route::get('/index', ['uses' => 'SalasController@index']);
	Route::get('/cadastrar', ['uses' => 'SalasController@cadastrar']);
	Route::get('/delete/{id}', ['uses' => 'SalasController@delete']);
	Route::post('/cadastrar', ['uses' => 'SalasController@cadastrar']);
});

Route::group(['prefix' => 'salas_itens'], function(){
	Route::get('/index', ['uses' => 'SalasItensController@index']);
	Route::get('/cadastrar', ['uses' => 'SalasItensController@cadastrar']);
	Route::get('/delete/{id}', ['uses' => 'SalasItensController@delete']);
	Route::post('/cadastrar', ['uses' => 'SalasItensController@cadastrar']);
});

Route::group(['prefix' => 'classes'], function(){
	Route::get('/index', ['uses' => 'ClassesController@index']);
	Route::get('/cadastrar', ['uses' => 'ClassesController@cadastrar']);
	Route::get('/delete/{id}', ['uses' => 'ClassesController@delete']);
	Route::post('/cadastrar', ['uses' => 'ClassesController@cadastrar']);
});

Route::group(['prefix' => 'usuarios_classes'], function(){
	Route::get('/index', ['uses' => 'UsuariosClassesController@index']);
	Route::get('/cadastrar', ['uses' => 'UsuariosClassesController@cadastrar']);
	Route::get('/delete/{id}', ['uses' => 'UsuariosClassesController@delete']);
	Route::post('/cadastrar', ['uses' => 'UsuariosClassesController@cadastrar']);
});

Route::group(['prefix' => 'ocorrencias'], function(){
	Route::get('/index', ['uses' => 'OcorrenciasController@index']);
	Route::get('/get_items_by_classe', ['uses' => 'OcorrenciasController@get_items_by_classe']);
	Route::get('/delete/{id}', ['uses' => 'OcorrenciasController@delete']);
	Route::get('/cadastrar', ['uses' => 'OcorrenciasController@cadastrar']);
	Route::post('/cadastrar', ['uses' => 'OcorrenciasController@cadastrar']);
});
