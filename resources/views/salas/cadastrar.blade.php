@extends('adminlte::page')

@section('title', 'Salas')

@section('content_header')
    <h1>CADASTRAR NOVA SALA</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>

    <form type="POST" action="{{URL::to('/salas/cadastrar')}}">
    	<div class="row">
    		<div class="col-sm-3">
                <div class="form-group">
                    <label class="form-label">Módulo</label>
                    <select name="modulo_id" class="form-control" required>
                        <option value=""> Selecione </option>
                        @foreach(App\Modulo::all() as $modulo)
                            <option value="{{$modulo->id}}">{{$modulo->nome}}</option>
                        @endForeach
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label class="form-label">Número da Sala</label>
                    <input type="number" name="numero" class="form-control" required>
                </div>
            </div>

            <div class="col-sm-5">
    			<div class="form-group">
    				<label class="form-label">Localização</label>
    				<input type="text" name="localizacao" class="form-control" required>
    			</div>
    		</div>
    	</div>
    	<input type="submit" name="Cadastrar" class="btn btn-success">
    </form>
   
    
@stop