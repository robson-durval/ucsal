@extends('adminlte::page')

@section('title', 'Salas')

@section('content_header')
    <h1>LISTAGEM DE SALAS CADASTRADASS</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>
    <p><a href="{{\URL::to('/salas/cadastrar')}}" class="btn btn-success"> Cadastrar nova sala </a></p>
    <div class="row">
    	<div class="col-sm-12">
    		<table class="table table-bordered table-hover dataTable" id="example2">
    			<thead>
    				<tr>
                        <td>Módulo</td>
                        <td>Número da Sala</td>
                        <td>Localização</td>
    					<td>Ações</td>
    				</tr>
    			</thead>
    			<tbody>
    				@foreach($salas as $sala)
	    				<tr>
                            <td>{{App\Modulo::find($sala->modulo_id)->nome}}</td>
                            <td>{{$sala->numero}}</td>
	    					<td>{{$sala->localizacao}}</td>
                            <td><a href="{{\URL::to('/salas/delete/'.$sala->id)}}" class="btn btn-danger">Remover</a></td>
	    				</tr>
    				@endForeach
    			</tbody>
    		</table>
    	</div>
    </div>
    
@stop