@extends('adminlte::page')

@section('title', 'Módulos')

@section('content_header')
    <h1>CADASTRAR NOVO MÓDULO</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>

    <form type="POST" action="{{URL::to('/modulos/cadastrar')}}">
    	<div class="row">
    		<div class="col-sm-3">
    			<div class="form-group">
    				<label class="form-label">Nome</label>
    				<input type="text" name="nome" class="form-control" required>
    			</div>
    		</div>
    	</div>
    	<input type="submit" name="Cadastrar" class="btn btn-success">
    </form>
   
    
@stop