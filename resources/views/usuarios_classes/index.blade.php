@extends('adminlte::page')

@section('title', 'usuários em Sala de Aula')

@section('content_header')
    <h1>USUÁRIOS VINCULADOS A UMA CLASSE</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>
    <p><a href="{{\URL::to('/usuarios_classes/cadastrar')}}" class="btn btn-success"> Vincular usuário a uma classe</a></p>
    <div class="row">
    	<div class="col-sm-12">
    		<table class="table table-bordered table-hover dataTable" id="example2">
    			<thead>
    				<tr>
                        <td>Usuário</td>
                        <td>Perfil do Usuário</td>
                        <td>Classe</td>
                        <td>Ações</td>
    				</tr>
    			</thead>
    			<tbody>
    				@foreach($usuarios_classes as $usuario_classe)
	    				<tr>
                            <td>{{App\User::find($usuario_classe->usuario_id)->name}}</td>
                            <td>
                                @if(App\User::find($usuario_classe->usuario_id)->profile == 1)
                                    Aluno
                                @elseif(App\User::find($usuario_classe->usuario_id)->profile == 2)
                                    Professor
                                @else
                                    Coordenador
                                @endIf            
                            </td>
                            <?php $classe = App\Classes::find($usuario_classe->classe_id); ?>
                            <?php $sala_classe = App\Sala::find($classe->sala_id); ?>
                            <td>Classe dia {{date("d/m/Y", strtotime($classe->data))}} sala Nº {{$sala_classe->numero}}</td>
                            <td><a href="{{\URL::to('/usuarios_classes/delete/'.$usuario_classe->id)}}" class="btn btn-danger">Remover</a></td>
	    				</tr>
    				@endForeach
    			</tbody>
    		</table>
    	</div>
    </div>
    
@stop