@extends('adminlte::page')

@section('title', 'Salas')

@section('content_header')
    <h1>CADASTRAR NOVA SALA</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>

    <form type="POST" action="{{URL::to('/usuarios_classes/cadastrar')}}">
    	<div class="row">
    		<div class="col-sm-3">
                <div class="form-group">
                    <label class="form-label">Usuário</label>
                    <select name="usuario_id" class="form-control" required>
                        <option value=""> Selecione </option>
                        @foreach(App\User::where('profile', 1)->orWhere('profile', 2)->get() as $usuario)
                            <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                        @endForeach
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label class="form-label">Classe</label>
                    <select name="classe_id" class="form-control" required>
                        <option value=""> Selecione </option>
                        @foreach(App\Classes::all() as $classe)
                            <?php $sala_classe = App\Sala::find($classe->sala_id); ?>
                            <option value="{{$classe->id}}"> Classe dia {{date("d/m/Y", strtotime($classe->data))}} sala Nº {{$sala_classe->numero}} </option>
                        @endForeach
                    </select>
                </div>
            </div>

    	</div>
    	<input type="submit" name="Cadastrar" class="btn btn-success">
    </form>
   
    
@stop