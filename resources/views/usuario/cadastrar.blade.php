@extends('adminlte::page')

@section('title', 'Usuários')

@section('content_header')
    <h1>CADASTRAR NOVO USUÁRIO</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>

    <form type="POST" action="{{URL::to('/usuarios/cadastrar')}}">
    	<div class="row">
    		<div class="col-sm-3">
    			<div class="form-group">
    				<label class="form-label">Nome</label>
    				<input type="text" name="name" class="form-control" required>
    			</div>
    		</div>

    		<div class="col-sm-3">
    			<div class="form-group">
    				<label class="form-label">E-mail</label>
    				<input type="email" name="email" class="form-control" required>
    			</div>
    		</div>

    		<div class="col-sm-3">
    			<div class="form-group">
    				<label class="form-label">Senha</label>
    				<input type="password" name="password" class="form-control" required>
    			</div>
    		</div>

    		<div class="col-sm-3">
    			<div class="form-group">
    				<label class="form-label">Perfil</label>
    				<select class="form-control" name="profile">
    					<option value="1">Aluno</option>
    					<option value="2">Professor</option>
    					<option value="3">Coordenador</option>
    				</select>
    			</div>
    		</div>
    	</div>
    	<input type="submit" name="Cadastrar" class="btn btn-success">
    </form>
   
    
@stop