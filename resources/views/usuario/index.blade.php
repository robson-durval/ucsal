@extends('adminlte::page')

@section('title', 'Usuários')

@section('content_header')
    <h1>LISTAGEM DE USUÁRIOS CADASTRADOS</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>
    <p><a href="{{\URL::to('/usuarios/cadastrar')}}" class="btn btn-success"> Cadastrar novo usuário </a></p>
    <div class="row">
    	<div class="col-sm-12">
    		<table class="table table-bordered table-hover dataTable" id="example2">
    			<thead>
    				<tr>
    					<td>Nome</td>
    					<td>E-mail</td>
                        <td>Perfil</td>
    					<td>Ações</td>
    				</tr>
    			</thead>
    			<tbody>
    				@foreach($users as $user)
	    				<tr>
	    					<td>{{$user->name}}</td>
	    					<td>{{$user->email}}</td>
	    					<td>{{$user::$perfils[$user->profile]}}</td>
                            <td><a href="{{\URL::to('/usuarios/delete/'.$user->id)}}" class="btn btn-danger">Remover</a></td>
	    				</tr>
    				@endForeach
    			</tbody>
    		</table>
    	</div>
    </div>
    
@stop