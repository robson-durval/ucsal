@extends('adminlte::page')

@section('title', 'Itens na Sala de Aula')

@section('content_header')
    <h1>ITENS NA SALA DE AULA</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>
    <p><a href="{{\URL::to('/salas_itens/cadastrar')}}" class="btn btn-success"> Vincular item a sala de aula</a></p>
    <div class="row">
    	<div class="col-sm-12">
    		<table class="table table-bordered table-hover dataTable" id="example2">
    			<thead>
    				<tr>
                        <td>Item</td>
                        <td>Sala de Aula</td>
                        <td>Ações</td>
    				</tr>
    			</thead>
    			<tbody>
    				@foreach($salas_itens as $salas_item)
	    				<tr>
                            <td>{{App\Item::find($salas_item->item_id)->nome}}</td>
                            <td>Nº {{App\Sala::find($salas_item->sala_id)->numero}}</td>
                            <td><a href="{{\URL::to('/salas_itens/delete/'.$salas_item->id)}}" class="btn btn-danger">Remover</a></td>
	    				</tr>
    				@endForeach
    			</tbody>
    		</table>
    	</div>
    </div>
    
@stop