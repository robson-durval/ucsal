@extends('adminlte::page')

@section('title', 'Salas')

@section('content_header')
    <h1>CADASTRAR NOVA SALA</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>

    <form type="POST" action="{{URL::to('/salas_itens/cadastrar')}}">
    	<div class="row">
    		<div class="col-sm-3">
                <div class="form-group">
                    <label class="form-label">Item</label>
                    <select name="item_id" class="form-control" required>
                        <option value=""> Selecione </option>
                        @foreach(App\Item::all() as $item)
                            <option value="{{$item->id}}">{{$item->nome}}</option>
                        @endForeach
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label class="form-label">Sala de Aula</label>
                    <select name="sala_id" class="form-control" required>
                        <option value=""> Selecione </option>
                        @foreach(App\Sala::all() as $sala)
                            <option value="{{$sala->id}}"> Nº {{$sala->numero}}</option>
                        @endForeach
                    </select>
                </div>
            </div>

    	</div>
    	<input type="submit" name="Cadastrar" class="btn btn-success">
    </form>
   
    
@stop