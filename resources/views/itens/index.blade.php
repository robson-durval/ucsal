@extends('adminlte::page')

@section('title', 'Usuários')

@section('content_header')
    <h1>LISTAGEM DE ITENS CADASTRADOS</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>
    <p><a href="{{\URL::to('/itens/cadastrar')}}" class="btn btn-success"> Cadastrar novo item </a></p>
    <div class="row">
    	<div class="col-sm-12">
    		<table class="table table-bordered table-hover dataTable" id="example2">
    			<thead>
    				<tr>
                        <td>Nome</td>
    					<td>Ações</td>
    				</tr>
    			</thead>
    			<tbody>
    				@foreach($itens as $item)
	    				<tr>
	    					<td>{{$item->nome}}</td>
                            <td><a href="{{\URL::to('/itens/delete/'.$item->id)}}" class="btn btn-danger">Remover</a></td>
	    				</tr>
    				@endForeach
    			</tbody>
    		</table>
    	</div>
    </div>
    
@stop