@extends('adminlte::page')

@section('title', 'Itens na Sala de Aula')

@section('content_header')
    <h1>LISTA DE CLASSES CADASTRADAS</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>
    <p><a href="{{\URL::to('/classes/cadastrar')}}" class="btn btn-success"> Cadastrar Classe</a></p>
    <div class="row">
    	<div class="col-sm-12">
    		<table class="table table-bordered table-hover dataTable" id="example2">
    			<thead>
    				<tr>
                        <td>Sala de Aula</td>
                        <td>Data</td>
                        <td>Disciplina</td>
                        <td>Observação</td>
                        <td>Ações</td>
    				</tr>
    			</thead>
    			<tbody>
    				@foreach($classes as $classe)
	    				<tr>
                            <td>Nº {{App\Sala::find($classe->sala_id)->numero}}</td>
                            <td>{{date("d/m/Y", strtotime($classe->data))}}</td>
                            <td>{{$classe->disciplina}}</td>
                            <td>{{$classe->observacao}}</td>
                            <td><a href="{{\URL::to('/classes/delete/'.$classe->id)}}" class="btn btn-danger">Remover</a></td>
	    				</tr>
    				@endForeach
    			</tbody>
    		</table>
    	</div>
    </div>
    
@stop