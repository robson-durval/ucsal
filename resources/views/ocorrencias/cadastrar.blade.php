@extends('adminlte::page')

@section('title', 'Salas')

@section('content_header')
    <h1>CADASTRAR NOVA OCORRÊNCIA</h1>
@stop

<?php $classes_array = App\UsuariosClasses::where(['usuario_id' => \Auth::user()->id])->get();

$ids = [];

foreach ($classes_array as $key => $value) {
    $ids[] = $value->classe_id;
}

$classes = App\Classes::whereIn('id', $ids)->get();

$ids = [];

foreach ($classes as $key => $value) {
    $ids[] = $value->sala_id;
}

?>


@section('content')
    <p>Navege entre as páginas no menu lateral</p>

    <form type="POST" action="{{URL::to('/ocorrencias/cadastrar')}}">
    	<div class="row">
    		<div class="col-sm-3">
                <div class="form-group">
                    <label class="form-label">Sala</label>
                    <select name="sala_id" id="sala_id" class="form-control" required>
                        <option value=""> Selecione </option>
                        @foreach(App\Sala::whereIn('id', $ids)->get() as $sala)
                            <option value="{{$sala->id}}">Sala Nº {{$sala->numero}}</option>
                        @endForeach
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label class="form-label">Item</label>
                    <select name="item_id" id="item_id" class="form-control" required>
                        <option value=""> Selecione </option>
                    </select>
                </div>
            </div>

            <div class="col-sm-5">
    			<div class="form-group">
    				<label class="form-label">Descrição</label>
    				<input type="text" name="descricao" class="form-control">
    			</div>
    		</div>
    	</div>
    	<input type="submit" name="Cadastrar" class="btn btn-success">
    </form>
   
    
@stop


@section('js')
    <script> 
        $(document).ready(function() {
            $(document).on('change', '#sala_id', function(){
                if( $(this).val() != "" ) {
                    $.ajax({
                        method: "GET",
                        url: "get_items_by_classe?classe_id="+$("#sala_id").val(),
                    }).done(function(data){
                        $("#item_id").html("");
                        $("#item_id").append(data);
                    });
                }else{
                    $("#occurrences-item_id").html("");
                }
            });
        });
    </script>
@stop