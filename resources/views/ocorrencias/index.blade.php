@extends('adminlte::page')

@section('title', 'Ocorrências')

@section('content_header')
    <h1>LISTAGEM DE OCORRÊNCIAS CADASTRADASS</h1>
@stop

@section('content')
    <p>Navege entre as páginas no menu lateral</p>
    @if(Auth::user()->profile == 1 || Auth::user()->profile == 2)
        <p><a href="{{\URL::to('/ocorrencias/cadastrar')}}" class="btn btn-success"> Cadastrar nova ocorrência </a></p>
    @endIf
    <div class="row">
    	<div class="col-sm-12">
    		<table class="table table-bordered table-hover dataTable" id="example2">
    			<thead>
    				<tr>
                        <td>Criado por</td>
                        <td>Perfil</td>
                        <td>Sala</td>
                        <td>Item</td>
                        <td>Descrição</td>
                        <td>Ações</td>
    				</tr>
    			</thead>
    			<tbody>
    				@foreach($ocorrencias as $ocorrencia)
	    				<tr>
                            <td>{{App\User::find($ocorrencia->criado_por)->name}}</td>
                            <td>
                                @if(App\User::find($ocorrencia->criado_por)->profile == 1)
                                    Aluno
                                @elseif(App\User::find($ocorrencia->criado_por)->profile == 2)
                                    Professor
                                @else
                                    Coordenador
                                @endIf 
                            </td>
                            <td>Sala Nº {{App\Sala::find($ocorrencia->sala_id)->numero}}</td>
                            <td>{{App\Item::find($ocorrencia->item_id)->nome}}</td>
                            <td>{{$ocorrencia->descricao}}</td>
                            <td><a href="{{\URL::to('/ocorrencias/delete/'.$ocorrencia->id)}}" class="btn btn-danger">Remover</a></td>
	    				</tr>
    				@endForeach
    			</tbody>
    		</table>
    	</div>
    </div>
    
@stop